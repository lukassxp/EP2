/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tocomfome.Model;

import java.util.ArrayList;

/**
 *
 * @author lpalu
 */
public class Pedido {
    
    private String nomeCliente;
    private String observacoes;
    private Double valor;
    private String formaPagamento;
    private Double valorPago;
    private Double troco;
    private ArrayList<Produto> refeicao;

    public Pedido(String nomeCliente, String observacoes, Double valor, String formaPagamento, Double valorPago, ArrayList<Produto> refeicao) {
        this.nomeCliente = nomeCliente;
        this.observacoes = observacoes;
        this.formaPagamento = formaPagamento;
        this.valorPago = valorPago;
        this.refeicao = refeicao;
    }

    public String getNomeCliente() {
        return nomeCliente;
    }

    public void setNomeCliente(String nomeCliente) {
        this.nomeCliente = nomeCliente;
    }

    public String getObservacoes() {
        return observacoes;
    }

    public void setObservacoes(String observacoes) {
        this.observacoes = observacoes;
    }

    public Double getValor() {
        return valor;
    }

    public String getFormaPagamento() {
        return formaPagamento;
    }

    public void setFormaPagamento(String formaPagamento) {
        this.formaPagamento = formaPagamento;
    }

    public Double getValorPago() {
        return valorPago;
    }

    public void setValorPago(Double valorPago) {
        this.valorPago = valorPago;
    }

    public Double getTroco() {
        return troco;
    }

    public ArrayList<Produto> getRefeicao() {
        return refeicao;
    }

    public void setRefeicao(ArrayList<Produto> refeicao) {
        this.refeicao = refeicao;
    }
    
}
