/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tocomfome.Model;

import java.util.ArrayList;

/**
 *
 * @author lpalu
 */
public class Estoque {
    
    private ArrayList<Produto> estoque;

    public Estoque(ArrayList<Produto> estoque) {
        this.estoque = estoque;
    }

    public ArrayList<Produto> getEstoque() {
        return estoque;
    }

    public void setEstoque(ArrayList<Produto> estoque) {
        this.estoque = estoque;
    }

    
}
